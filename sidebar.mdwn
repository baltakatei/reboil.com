- <a rel="me" href="https://twit.social/@baltakatei">Mastodon</a>

- [Lemmy](https://sopuli.xyz/u/baltakatei)

- [Notable Public Keys](https://reboil.com/res/2023/txt/20230624T1919+00..notable_public_keys_v0.4.0.pdf)

- [Thermodynamics and Chemistry](https://reboil.com/res/2022/txt/20220320T1802Z..devoe_thermo_book_retypeset_draft.pdf)

- [Wikimedia Commons](https://commons.wikimedia.org/w/index.php?title=Special:ListFiles&user=Baltakatei)

- [TeXmacs articles](https://reboil.com/texmacs/)

- [Reboil.com wiki](https://reboil.com/mediawiki/)

[[!if test="enabled(calendar)" then="""
[[!calendar pages="page(./posts/*) and !*/Discussion"]]
"""]]

[[Recent Comments|comments]]

[[Archives]]

[[Tags]]:
[[!pagestats style="list" pages="./tags/*" among="./posts/*"]]


