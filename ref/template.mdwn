<meta charset="utf-8">

# Title

Created by [Steven Baltakatei Sandoval][bktei_2020_homepage] on <iso-8601> under a [CC BY-SA 4.0][cc_20131125_bysa] license and last updated on <iso-8601>.

## Body

Some claim.<sup>[[1]](#times_20090103_bailout)</sup>

### Subsection

## References
- <a name="times_20090103_bailout">1.</a> Elliott, Francis (2009-01-03). ["Chancellor on brink of second bailout for banks"][1]. [The Times](https://en.wikipedia.org/wiki/The_Times). Date Accessed: 2020-04-24. [Archive link](https://web.archive.org/web/20170404045527/http://www.thetimes.co.uk/article/chancellor-alistair-darling-on-brink-of-second-bailout-for-banks-n9l382mn62h). Archive date: 2017-04-04.

[1]: https://www.thetimes.co.uk/article/chancellor-alistair-darling-on-brink-of-second-bailout-for-banks-n9l382mn62h
[bktei_2020_homepage]: http://baltakatei.com
[cc_20131125_bysa]: http://creativecommons.org/licenses/by-sa/4.0/


<hr>
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#">This work by <a rel="cc:attributionURL"  href="http://baltakatei.com"><span rel="cc:attributionName">Steven Baltakatei Sandoval</span></a> is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser" target="_blank" rel="license noopener noreferrer" style="display: inline-block;">CC BY-SA 4.0</a><a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser"><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-by_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-sa_icon.svg" /></a></p>
