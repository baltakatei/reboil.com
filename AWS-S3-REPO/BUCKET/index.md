<head>
<title>reboil.com</title>
</head>

# reboil.com

Created by [Steven Baltakatei Sandoval][bktei_2020_homepage] on 2019-07-03T17:14Z under a [CC BY-SA 4.0][cc_20131125_bysa] license and last updated on 2020-09-19T07:04Z.

## Creative Works

### Text

- [002019-07-05..forgetful](fiction/0020190705T155632Z..forgetful.txt)

### Image

- [002020-02-07..Chocobo Crossing Sign](http://baltakatei.com/WORKS/20200207T2133Z..winhill_chocobo_crossing_sign.pdf) ([png](http://baltakatei.com/WORKS/20200207T2133Z..winhill_chocobo_crossing_sign.png),[svg](http://baltakatei.com/WORKS/20200207T2133Z..winhill_chocobo_crossing_sign.svg))

[Licenses for my creative works](http://baltakatei.com/LICENSES/README.html).

## Blog

- [002019-07-16..Citation Needed hunt](blog/0020190716T142258Z..citation_needed_hunt.txt)
- [002019-07-18..Russian Roulette etymology](blog/0020190718T032951Z..russian_roulette.txt)
- [002019-07-18..Kyoto Animation arson attack](blog/0020190718T232014Z..kyoani_arson_attack.txt)
- [002019-07-28..Markup notes.html](blog/0020190728T194825Z..markup.html)
- [002019-08-09..Four Freedoms, Three Purposes](blog/0020190809T180929Z..four_freedoms_three_purposes.html)
- [002019-08-15..Hancke-Kuhn Distance-Bounding Protocol Explanation](blog/0020190815T064636Z..hancke-kuhn_dbp_explanation.html)
- [002019-08-16..Automatic `ssh` Firefox Proxy Script, Lessons Learned](blog/0020190816T050642Z..auto_ssh_firefox_proxy_script.html)
- [002020-01-27..Investigating some `apt-get` warnings](blog/0020200127T153309Z..debian_buster_apt_keyrings.html)
- [002020-01-30..Google Earth `.deb` installer PGP keys needing updating](blog/0020200130T171327Z..google_earth_deb_pgp_key.html)
- [002020-02-14..Creating split backups with `rsync` and `tar`](blog/0020200214T194108Z..backing_up_large_filesystem.html)
- [002020-02-17..Spanish lyrics for Final Fantasy: Pray](blog/0020200217T103218Z..final_fantasy_pray_lyrics_spanish.html)
- [002020-05-24..Stack Exchange answer: Time required to heat mass in constant-volume process](doc/0020200524T003849Z..time_required_to_heat_mass_in_constant-volume_process.pdf)
- [002020-09-19..Cost of industry consensus standards for chemical plant drawings](calc/0020200919T070405Z..chemical_plant_drawing_standards.ods)

<hr>

[bktei_2020_homepage]: http://baltakatei.com
[cc_20131125_bysa]: http://creativecommons.org/licenses/by-sa/4.0/

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#">This work by <a rel="cc:attributionURL"  href="http://baltakatei.com"><span rel="cc:attributionName">Steven Baltakatei Sandoval</span></a> is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser" target="_blank" rel="license noopener noreferrer" style="display: inline-block;">CC BY-SA 4.0</a><a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser"><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-by_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-sa_icon.svg" /></a></p>
