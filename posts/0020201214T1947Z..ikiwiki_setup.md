<meta charset="utf-8">

# Ikiwiki Setup

Created by [Steven Baltakatei Sandoval][bktei_2020_homepage] on
2020-12-14T19:48Z under a [CC BY-SA 4.0][cc_20131125_bysa] license and
last updated on 2020-12-14T20:28Z.

## Background

I decided to add TLS functionality to my personal blog at a domain
that I own `https://reboil.com`. For the past few years it has been a
static markdown-based git-versioned website hosted in an Amazon S3
bucket using Amazon's Route 53 service to permit webpages to appear
like they are being fetched from `reboil.com`. However, this setup
does not permit pages to be secured by TLS encryption as most webpages
are. Therefore, a few years ago I decided to look for a method of
posting my blog in such a way that was secured by TLS.

Many blogging services exist if you are okay with the encryption keys
being hosted by servers you don't control. Some blogging software
permits individuals to avoid using third-party servers (ex:
Wordpress).

I forget where exactly I heard of it, but I found that a
hardware-software package called
"[Freedombox](https://www.freedomboxfoundation.org/)" existed with the
intention of permitting individuals to replace third parties necessary
for common "cloud" services such as file-sharing, chatting,
[git](https://git-scm.com/) repository hosting, and blogging. One
service that a "Freedombox" provides is blogging via
[Ikiwiki](https://ikiwiki.info/).

I found that the Ikiwiki instance that Freedombox installs into itself
(I find the web user interface convenient) comes preconfigured to
version control posts using `git`. Also, posts can be made by creating
a [markdown](https://daringfireball.net/projects/markdown/) text file
in a particular directory followed by running the `git push` command
as the `root` user in Freedombox.

I also found that a Freedombox can automatically create its own TLS
certificates and get them signed via Let's Encrypt so most web
browsers can communicate with the Freedombox through a connection
secured by encryption. Let's Encrypt certificates are private-public
key pairs whose private keys are always retained on the local machine
and not shared with any third-parties! This meant that I was able to
fulfill my goal of creating a blogging server physically located
within my own room. With the [Freedombox's Dynamic DNS
service](https://wiki.debian.org/FreedomBox/Manual/DynamicDNS) and
[FreeDNS](freedns.afraid.org/), I am able to also make sure even the
occasional public IP change caused by my internet service provider's
dynamic IP allocation algorithm won't interrupt availability of my
blog for long.

Since a Freedombox is basically a headless Debian machine, experience
I have gained since 2017 in using Debian directly apply to help
me. For example, I learned how to install packages via `apt` which I
found works on a Freedombox. I learned how to log remotely into a
headless Debian machine via `ssh` and a [PGP
subkey](https://blog.eleven-labs.com/en/openpgp-almost-perfect-key-pair-part-1/)
loaded onto a USB smartcard. I learned how to use
[emacs](https://www.emacswiki.org/) and [magit](https://magit.vc/) to
synchronize my `git` commits with a remote server (another function a
Freedombox provides via
[gitweb](https://wiki.debian.org/FreedomBox/Manual/GitWeb)). I learned
how to version control my dotfiles using [yadm](https://yadm.io/). I
learned how to use git to version control my static websites such as
my existing blog. I learned recently that Ikiwiki's `git`
compatibility also theoretically permits me to maintain a continuity
of my existing blog git repo in a new repo (I imagine I'll save and
commit a `git` bundle of my old blog into the new Ikiwiki git repo).

So, all this has led me to be confident enough to move my blog to this
ikiwiki blog. Since my old blog posts use markdown and Ikiwiki permits
posts to be published via `git push` and creation of markdown files in
a specific directory, I should be able to easily migrate my existing
static webpage blog posts to ikiwiki. `:D`.

## Future action items

Today the blog has been moved. Now I just need to construct a workflow
that will allow me to sign my blog commits with my personal PGP
smartcard. I think one way would be to:

1. Initially mirror the ikiwiki blog git repo to a remote server (ex:
my personal Freedombox gitweb server).

2. Set up a `cron` job that automatically and periodically pulls
commits from my personal remote git server and pushes them so ikiwiki
is triggered to render the markdown files into static webpages that
are publicly available.

3. Clone the blog git repo on my personal server to a personal
workstation (or three). Using the USB smartcard plugged into my
personal workstation, I can create, commit, sign, and push blog posts
to my personal git server.

Thus, I will won't have to plug my USB smartcard into the physical
Freedombox. All posts can be signed with my PGP key, reducing the
probability that posts are made by an impostor and increasing the
value of my observations that I publish to my blog.

[bktei_2020_homepage]: http://baltakatei.com
[cc_20131125_bysa]: http://creativecommons.org/licenses/by-sa/4.0/


<hr>
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#">This work by <a rel="cc:attributionURL"  href="http://baltakatei.com"><span rel="cc:attributionName">Steven Baltakatei Sandoval</span></a> is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser" target="_blank" rel="license noopener noreferrer" style="display: inline-block;">CC BY-SA 4.0</a><a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser"><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-by_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-sa_icon.svg" /></a></p>
