* Ikiwiki Org mode
#+TITLE: Ikiwiki Org mode
#+AUTHOR: Steven Baltakatei Sandoval
#+DATE:2021-02-16
#+EMAIL:baltakatei@gmail.com
#+LANGUAGE: en
#+OPTIONS: toc:nil

Created by [[http://baltakatei.com][Steven Baltakatei Sandoval]] on
[[https://reboil.com/mediawiki/2021-02-16][2021-02-16]]T16:10+00
under a [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]] license and last updated on
2023-05-31T19:46+00.

Edit([[https://reboil.com/mediawiki/2023-05-31][2023-05-31]]):Add reboil.com wiki links.

** Summary
I explored the possibility of enabling a plugin in Ikiwiki that could
allow me to compose blog posts in Org mode format without having to
bother with re-exporting to markdown format. I judged that the best
plugin I found required too many complexity for the convenience I
would receive. Therefore, I did not follow through but I recorded my
research findings. I am publishing these details because were I not
limited by the fact that my Ikiwiki blog runs as root within a
Freedombox, I would probably would have made use of an org mode plugin
I found.  running Ikiwiki within a FreedomBox.

** Background
I have been interested in composing blog posts using Emacs [[https://orgmode.org/][Org mode]]
since I already use Emacs and Org mode offers some features that
aren't present in regular markdown.

Through various searches I saw that an org mode plugin exists for
[[https://ikiwiki.info/][Ikiwiki]], the software I am using to render my static blog. The Ikiwiki
instance I use runs on a [[https://freedombox.org/][FreedomBox]].

I began documenting my research into various org mode plugins for
Ikiwiki. Ultimately I decided not to continue due to how Ikiwiki runs
as root in FreedomBox, but I include this information to help others
who may find the details useful in their particular use cases.

** Setup
*** Identify Ikiwiki's Perl directory
According to [[https://ikiwiki.info/plugins/install/][this]] page, "most ikiwiki plugins are perl modules and
should be installed somewhere in the perl module search path." The
page recommends I run ~perl -V~ and pay attention to the paths listed
after a ~@INC~ line. It also said that all plugins are a
"Ikiwiki/Plugin subdirectory inside the perl search path".

For my Freedombox machine in which ikiwiki runs, I get the following
results.

#+BEGIN_EXAMPLE
# perl -V | tail -n10
  @INC:
    /etc/perl
    /usr/local/lib/arm-linux-gnueabihf/perl/5.28.1
    /usr/local/share/perl/5.28.1
    /usr/lib/arm-linux-gnueabihf/perl5/5.28
    /usr/share/perl5
    /usr/lib/arm-linux-gnueabihf/perl/5.28
    /usr/share/perl/5.28
    /usr/local/lib/site_perl
    /usr/lib/arm-linux-gnueabihf/perl-base
#+END_EXAMPLE

After running a few ~find~ commands looking for directories containing
"ikiwiki", I found only ~/usr/share/perl5/~ did:

#+BEGIN_EXAMPLE
# find /usr/share/perl5 -iname "*ikiwiki*"
/usr/share/perl5/IkiWiki
/usr/share/perl5/IkiWiki.pm
#+END_EXAMPLE

This is how I found the ~/usr/share/perl5/IkiWiki/Plugin~ directory
that contains various ~.pm~ files that look like default Ikiwiki
plugins. Presumably, I install a custom plugin by copying a ~.pm~ file
into this directory.

*** Identify plugin file to install
The ikiwiki website has a page about the [[https://ikiwiki.info/plugins/contrib/org_mode/][org mode plugin]].

However, the page does not provide a clear image of which plugin
should be used. The page content takes the form of a discussion with
no clear authorship indicated for any part. Also, the "[[https://ikiwiki.info/plugins/contrib/org_mode/Discussion/][Discussion]]"
page is [[https://ikiwiki.info/bugs/Existing_Discussion_pages_appear_as_non-existing][bugged]], apparently.

After following up several leads, it appears there are three org mode
plugins for ikiwiki.

**** Manoj plugin
The first is an original one created and [[https://web.archive.org/web/20080908055926/http://www.golden-gryphon.com/software/misc/org.pm.html][published]] by Manoj
Srivastava.

It is dated [[https://reboil.com/mediawiki/2008-06-08][2008-06-08]] (file timestamp).

**** W. Trevor King plugin
The second is a version of Manoj's plugin updated by [[http://blog.tremily.us][W. Trevor King]]
(a.k.a. ~wtk~, ~wking~; [[http://blog.tremily.us][their blog]]) published on their github [[https://github.com/wking/ikiwiki/blob/org/IkiWiki/Plugin/org.pm][here]]
(under their ~ikiwiki~ repository under the ~org~ branch and at
~Ikiwiki/Plugin/org.pm~ within the directory tree). This version has
problems handling "wikilinks and other ikiwiki markup".

It is dated [[https://reboil.com/mediawiki/2010-10-06][2010-10-06]] (git commit date).

**** Chris M. Gray plugin
The third is an original rewrite authored by Chris M. Gray and
[[https://github.com/chrismgray/ikiwiki-org-plugin][published]] on their github site. It requires modification of local
~emacs~ configuration files in order to function properly but claims
to achieve faster performance by avoiding starting multiple instances
of ~emacs~ which previous plugins did.

I decided this was the best plugin to install since it was the most
recently updated.

It requires installation of two files from the git [[https://github.com/chrismgray/ikiwiki-org-plugin][repository]]:

- ~perl/new_org.pm~: Installable to the ~Ikiwiki/Plugin~ directory
  within the local machine's perl module search path. In my case this
  was the ~/usr/share/perl5/Ikiwiki/Plugin~ directory.
- ~lisp/ikiwiki-org-plugin.el~: Installable to the ~load-path~ of an
  ~emacs~ daemon so ~emacsclient~ instances can directly render ~org~
  files to ~html~. In my case it would require modifying my
  ~/root/.emacs.d/init.el~ to add ~/root/.emacs.d/ikiwiki_plugins~ to
  the ~emacs~ ~load-path~ (see [[https://www.emacswiki.org/emacs/LoadPath][ref]]).

After some initial tests, I concluded this method was not appropriate
for Ikiwiki running on a Freedombox since it requires that an Emacs
daemon be running in order to make ~emacsclient~ available (which
presumably is the feature that prevents multiple separate instances of
~emacs~ from starting up). The minor benefits of automatic rendering
of org mode files into HTML without the intermediate step of manually
exporting org to markdown is not worth the security issues of
requiring an emacs daemon (an all its plugins) to run as root.

The plugin is dated [[https://reboil.com/mediawiki/2016-04-01][2016-04-01]] (git commit date of elisp module).

*** Install
**** Install plugin files
I cloned the git repository to obtain the required files and copied
the files as described earlier into their appropriate directories.

#+BEGIN_EXAMPLE
# pushd /tmp
# git clone https://github.com/chrismgray/ikiwiki-org-plugin ikiwiki-org-plugin
# cp ikiwiki-org-plugin/perl/new_org.pm /usr/share/perl5/Ikiwiki/Plugin/
# cp ikiwiki-org-plugin/lisp/ikiwiki-org-plugin.el ~/.emacs.d/ikiwiki_plugins/
#+END_EXAMPLE

**** Configure ~blog.setup~
Continuing the installation procedure described in Chris M. Gray's
~ikiwiki-org-plugin~ repository's ~README.org~ file, the next step is
to configure the appropriate blog or wiki's ~.setup~ file to enable
the ~new_org~ plugin.

In my case, the ~.setup~ files are located in ~/var/lib/ikiwiki/~; the
change involved changing this list:

#+BEGIN_EXAMPLE
add_plugins:
- goodstuff
- websetup
- comments
- calendar
- sidebar
- trail
- httpauth
- lockedit
- opendiscussion
- moderatedcomments
- userlist
- remove
- attachment
- underlay
#+END_EXAMPLE

to:

#+BEGIN_EXAMPLE
add_plugins:
- goodstuff
- websetup
- comments
- calendar
- sidebar
- trail
- httpauth
- lockedit
- opendiscussion
- moderatedcomments
- userlist
- remove
- attachment
- underlay
- new_org
#+END_EXAMPLE

** Test
I tested the setup procedure on a test blog with this command:

: # ikiwiki --setup /var/lib/ikiwiki/blog-test.setup --rebuild --verbose --gettime

The result was an error message from ~emacsclient~ indicating that an
~emacs~ daemon was not detected. This was the point in my research
where I decided attempting to use an org mode plugin with Ikiwiki was
not worth the trouble. I knew how to enable an ~emacs~ daemon running
with user permissions on my Debian 10 machines. However, the prospect
of running ~emacs~ as root unmonitored and automatically (since I have
a script that automatically runs Ikiwiki to regularly rebuild my
static blog site) did not appeal to me. I am familiar with running
emacs instances as root to modify configuration files but never to
perform automatic transformations of text.

* Conclusion
The best Ikiwiki plugin to enable direct rendering of HTML pages from
org mode source requires an ~emacs~ daemon to be
running. Unfortunately, the complexity of configuring such a daemon to
support Ikiwiki running as the ~root~ user on my Freedombox serving my
personal blog was too much for me to be comfortable with.
