* Freedombox Static Website Research
#+TITLE: Freedombox Static Website Research
#+AUTHOR: Steven Baltakatei Sandoval
#+DATE:2021-02-17
#+EMAIL:baltakatei@gmail.com
#+LANGUAGE: en
#+OPTIONS: toc:nil

Created by [[http://baltakatei.com][Steven Baltakatei Sandoval]] on
2021-02-17T22:32Z
under a [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]] license and last updated on
2023-03-04T14:19+00
.

Edit(2023-03-04T14:19+00):Updated ikiwiki blog repository URL.

#+BEGIN_COMMENT
- Export his file to HTML in Emacs using C-c C-e h o

- When exporting this file to HTML, add the "#+OPTIONS: html-postamble:nil"
  line to the document to avoid including a W3C validation link.

- See the Org mode manual to understand the structure of this .org
  file ( https://orgmode.org/orgguide.pdf ).
#+END_COMMENT

** Background
I have been investigating other possible methods for publishing
content accessible under my ~reboil.com~ domain. Currently, this
ikiwiki blog is accessible via:
https://reboil.com/gitweb/BK-2020-08-1.git . However, the potential
for formatting is not great; I would not use this alone when
publishing mathematical equations, for example (i.e. [[https://texmacs.org][texmacs]]).

The [[https://freedombox.org][Freedombox]] I own and run this blog off of has been great for
introducing me to the concepts of securing my own personal webpages
served by [[https://wiki.debian.org/Apache][Apache]]. It permits me to publish wiki and blog content via
[[https://wiki.debian.org/FreedomBox/Manual/MediaWiki][Mediawiki]] or [[https://wiki.debian.org/FreedomBox/Manual#FreedomBox.2FManual.2FIkiwiki.Ikiwiki_.28Wiki_and_Blog.29][Ikiwiki]]. Although I am familiar with the [[https://en.wikipedia.org/wiki/Help:Wikitext][wikitext]] markup
of Mediawiki thanks to some time I have spent editing [[https://en.wikipedia.org][Wikipedia]] pages,
I prefer simpler solutions that don't involve accepting public input
in the form of comments or account registration. I just want to be
able to publish my own works.

** Investigations
I recently investigated how I could use [[https://orgmode.org/][org mode]] (a note organization
application within [[https://www.gnu.org/software/emacs/][Emacs]]) to automatically render HTML pages for
serving within an Ikiwiki blog. However, I ultimately decided that
none of the org mode plugins for Ikiwiki were suitable for me.

I later investigated the possibility of using a mathematical
typesetting program called TeXmacs to render a static website using
its own WYSIWYG interface. The disadvantage of authoring webpages in
TeXmacs is that authoring the source files (file extension ~.tm~)
properly requires running the graphical WYSIWYG interface in order to
immediately see the typesetting results. Markdown, by contrast, is a
format in which the source is also the text. TeXmacs itself has a
[[https://texmacs.gitee.io/notes/docs/website-builder-dialog.html][static website generator function]] that takes a source directory full
of ~.tm~ files and outputs rendered ~.xhtml~ files viewable by a web
browser; CSS preferences are set in the TeXmacs Preferences
settings. Some built-in CSS preferences make the resulting webpage
appear quite nice. The disadvantage of this method is that quick
authoring of blog posts requires firing up TeXmacs to render new a new
~.tm~ source file for each blog post I compose.

I also saw that FreedomBox developers have decided to add Wordpress as
an app alongside Ikiwiki as part of their [[https://wiki.debian.org/FreedomBox/Roadmap2021][2021 Roadmap]]. A discussion
on the forum indicates this decision was made due to user feedback
that publishing a website on the FreedomBox still requires some
technical know-how regarding GNU/Linux file permissions and modifying
configuration files via the command line interface through an ~ssh~
connection. I'm reminded of my time messing with Ikiwiki's
~/var/lib/ikiwiki/blog.setup~ configuration files in order to enable
or disable built-in plugins. I am wary of using Wordpress, since
popular plugins for it have been a regular source of security breaches
according to my time listening to the [[https://www.grc.com/SecurityNow.htm][Security Now]] podcast I have been
following for years.

** Proposal
So, for now, I think I will stick to using Ikiwiki for composing
simple text-only blog posts in org mode and then converting them to
markdown for Ikiwiki to process. However, if ever images or
mathematical equations need to be published, I think I will create a
static website using TeXmacs and serve it under my ~reboil.com~
Freedombox via a root ~cron~ job that ~git pull~'s a repo containing
the TeXmacs site generator output and ~rsync~'s select parts of the
repository to the FreedomBox's ~/var/www/html/~ directory.

~blog~ seems appropriate for the Ikiwiki site I have since it implies
a "log", a stream of ideas that don't necessarily contain essential
structured information. However, the TeXmacs pages I make will, by
their nature, be capable of much more custom formatting thanks to
TeXmac's deep MathML support and pleasant typesetting features
(headers, equation numbering, image linking, etc.). Therefore, also
calling the TeXmacs static web site a "blog" seems
inappropriate. "Notes", "Articles", "Analects", or "Documents" seem
more appropriate to describe what TeXmacs produces when rendering
source ~.tm~ files. I like "Articles", since it invoke the idea of
"newspaper articles" or "column articles"; basically, relatively
independent parts of a larger typeset publication. This 1913
definition from the Webster dictionary highlights the meaning I'd like
to emphasize:

#+BEGIN_QUOTE
  Article \Ar"ti*cle\, n. [F., fr. L. articulus, dim. of artus
     joint, akin to Gr. ?, fr. a root ar to join, fit. See {Art},
     n.]
     1. A distinct portion of an instrument, discourse, literary
        work, or any other writing, consisting of two or more
        particulars, or treating of various topics; as, an article
        in the Constitution. Hence: A clause in a contract, system
        of regulations, treaty, or the like; a term, condition, or
        stipulation in a contract; a concise statement; as,
        articles of agreement.
        [1913 Webster]
  
     2. A literary composition, forming an independent portion of
        a magazine, newspaper, or cyclopedia.
        [1913 Webster]
#+END_QUOTE

** Project code update
Here is a set of project codes related to my reboil.com static
website.

*** BK-2020-08: Ikiwiki blog
- [[https://reboil.com/gitweb/BK-2020-08-1.git][Git repository]]
- [[https://reboil.com/ikiwiki/blog/][Link]]
*** BK-2020-08-2: Ikiwiki blog binary blobs
- [[https://reboil.com/gitweb/BK-2020-08-2.git][Git repository]]
*** BK-2020-08-3: TeXmacs articles
- [[https://reboil.com/gitweb/BK-2020-08-3.git][Git repository]]
*** BK-2020-08-4: TeXmacs articles binary blobs
- [[https://reboil.com/gitweb/BK-2020-08-4.git][Git repository]]
- Note: a submodule of the BK-2020-08-3 git repository.

** Conclusion
I think I will call my TeXmacs-powered static website ~articles~, as
in "articles of a newspaper" or, more ambitiously "articles of an
academic journal". I will host it at ~reboil.com/articles/~, probably
using a ~cron~ job in my Freedombox to automatically ~rsync~ article
files rendered and committed to a git repository.
