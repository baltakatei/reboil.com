* Notable Public Keys book update (Debian, Satoshi Labs)
#+TITLE: Notable Public Keys book update (Debian, Satoshi Labs)
#+AUTHOR: Steven Baltakatei Sandoval
#+DATE:2021-01-03
#+EMAIL:baltakatei@gmail.com
#+LANGUAGE: en
#+OPTIONS: toc:nil

Created by [[http://baltakatei.com][Steven Baltakatei Sandoval]] on
2022-01-03T12:58Z
under a [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]] license and last updated on
2022-01-03T13:26Z.

#+BEGIN_COMMENT
- Export his file to HTML in Emacs using C-c C-e h o

- When exporting this file to HTML, add the "#+OPTIONS: html-postamble:nil"
  line to the document to avoid including a W3C validation link.

- See the Org mode manual to understand the structure of this .org
  file ( https://orgmode.org/orgguide.pdf ).
#+END_COMMENT

I added two sections to my Notable Public Keys book ([[https://reboil.com/res/2022/txt/20220103T1236Z..pubkeys_book_draft.pdf][PDF]], [[https://gitlab.com/baltakatei/bk-2021-09][GitLab]]):

- [[https://debian.org][Debian]]
- [[https://satoshilabs.com][Satoshi Labs]]

** Debian
For the Debian chapter I focused on PGP keys used by the "Debian CD"
group to sign ~.iso~ images used to install Debian onto new
systems.

I included this chapter because I use Debian as my main workstation
OS. I also know that it is used as the basis for several other popular
GNU/Linux distributions such as [[https://ubuntu.com/][Ubuntu]] and some that I've been looking
into using such as [[https://pop.system76.com/][PopOS]].

Because the Debian organization continues to use GnuPG keys as the
basis for officially authenticating developer contributions, their use
of PGP keys goes back longer than most organizations covered by this
book (the oldest Debian CD key I found is dated 1999-01-30).

** Satoshi Labs
For the Satoshi Labs chapter I included two recent keys used to sign
[[https://trezor.io][Trezor]] software such as [[https://wiki.trezor.io/Apps:Trezor_Suite][Trezor Suite]] and [[https://wiki.trezor.io/Trezor_Bridge][Trezor Bridge]]. One of the
Satoshi Labs founders named Stick also created signatures included
alongside software such as Trezor Bridge. Currently the latest
[[https://blog.trezor.io/trezor-suite-launches-8958c1d37d33][recommended]] method for using a Trezor hardware wallet is to use Trezor
Suite.

I included this chapter because I use a Trezor to store
[[https://en.wikipedia.org/wiki/Bitcoin][bitcoin]]. Because money is at stake I have maintained notes about PGP
keys used to sign Trezor software. In fact, one of the reasons why I
decided to make the book was to gather all my notes into a single
coherent text.

** Future sections
In the project ~README~ I have the following entities whose public
keys I am still planning to include in their own sections:

- [[https://electrum.readthedocs.io/en/latest/gpg-check.html][Electrum]]
- [[https://f-droid.org/en/docs/Release_Channels_and_Signing_Keys/][F-Droid]]
- [[https://ask.libreoffice.org/t/how-do-i-verify-my-lo-installer-where-are-the-public-keys/49021/5][LibreOffice]]
- [[https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/security_guide/sect-security_guide-updating_packages-verifying_signed_packages][Red Hat]]
- [[https://tails.boum.org/contribute/build/reproducible/#index1h3][TailsOS]]
- [[https://support.torproject.org/tbb/how-to-verify-signature/][Tor Browser]]
- [[https://sourceforge.net/p/veracrypt/discussion/general/thread/fcd0da57/][Veracrypt]]
