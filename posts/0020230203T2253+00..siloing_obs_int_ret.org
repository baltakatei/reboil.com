* Siloing of Observation, Integration, and Reticulation
#+TITLE: Siloing of Observation, Integration, and Reticulation
#+AUTHOR: Steven Baltakatei Sandoval
#+DATE:2023-02-03
#+EMAIL:baltakatei@gmail.com
#+LANGUAGE: en
#+OPTIONS: toc:nil

Created by [[https://baltakatei.com][Steven Baltakatei Sandoval]] on
2023-02-03T22:55+00
under a [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]] license and last updated on
2023-02-04T03:32+00

** Summary
This is a rather undirected thought about how one could prevent an
intelligent being from emerging from an unintelligent system by
restricting or siloing certain categories of activities.

** Background
In an earlier blog post titled "[[https://reboil.com/ikiwiki/blog/posts/0020190809T180929Z..four_freedoms_three_purposes/][Four Freedoms and Three Purposes]]", I
talked about my introspective notion that there are three universal
categories of activities that all intelligent life must engage in to
be conscious. I defined the three activities as:

#+begin_quote
1. 👁 **Observe and Discover** - To see reality as it really is. To take in
new givens.

2. 📚 **Integrate and Correlate** - To create stories explaining what you
see. To record history. To correlate givens with other givens.

3. 🖧 **Reticulate and Network** - To trade stories with entities different
from yourself. To form relationship networks with others.
#+end_quote

In addition, the following fourth self-evident action must also be included in the
list of activities since it's necessary for pruning away waste
products created like how subtractive lithography is required by a
stonecarver to etch information into stone.

#+begin_quote
* 💣 **Destroy and Forget** - To nullify an action.
#+end_quote

Over the years I occasionally step back from my activities and what I
see to try and categorize events and phenomena according to the
categories I defined. Here are some examples:

- Social media (e.g. Facebook, now Meta): Connecting people to deliver
  them news of the now. Although the dominant corporate hosts of this
  activity feed off advertising revenue that is augmented by the
  construction of predictive models of user minds based off their
  personal preferences and purchase histories (**integration and
  correlation**), I would argue the activity of connecting other
  people based on their current emotional states is primarily a
  **reticulate and network** activity. To the extent that echo
  chambers prevail, social media isn't an **observe and discover**
  activity; echo chambers, by definition, are dispelled when outside
  information pervades the space.

- Law: Interpretation of the law is a political activity in which dead
  authorities who wrote laws cannot defend themselves except through
  their writing. Law is a shared hallucination in the minds of people
  that has power so long as people believe it has power; this is not
  to say that law must be abolished: law, as an idea, is essential as
  the idea that life is important, consequences have actions, and keep
  those kids away from that hot stove because they're idiots and will
  burn themselves and who will be the one rocking them to sleep with
  their burned fingers keeping them up? Lawyers must engage in
  **integration and correlation** to ascertain precedent so
  punishments and contract terms are applied equitably across space
  and time. However, time and space change: the political party whose
  members wrote a law is extinct; what once was a rational number of
  representatives for a past populace is now unworkable; suddenly
  social media accelerates protest organization from a matter of years
  to days. In other words, lawyers are tasked with **integrating and
  correlating** givens of the past with **observations and discoveries
  of the present**. As of 2023, the practice of law doesn't seem to be
  priamrily a **reticulation and networking** activity since strict
  laws exist to limit the flow of information between lawyers, their
  clients, and juries as they perform their work).

As I considered activities and asked myself which purpose each was
primarily dedicated towards, I had to ask myself questions such as:
 - How would a social media network function if each person was
   separated by a year of time delay?
 - Could law work without a case history to provide precedent?
 - What if case history was available but only for isolated clans of
   lawyers who were unable to share notes with one another?
 - What if lawyers could share case history with one another but they
   were prohibited from using recent observations and discoveries?
 - What would a social media network look like if content posted by
   users was strictly deleted after a day?

I was flipping switches of my mental model on and off in different
combinations and comparing what came out with what I had seen of the
world. I soon realized most of these attempted counterfactuals did
exist in fact, to some extent:

- A social media network with message auto-delete functions? Signal.
- Clans of lawyers unable to share notes with one another? Basically
  how law was practiced in individual city-states before modern
  communications and, to some extent, in modern nation-states where
  language and jurisdiction barriers exist.
- Lawyers who can't use recent observations and discoveries? That's
  basically the common requirement in democractic governments that
  legislative bodies write the most important laws, not the lawyers
  (at least, not on-the-fly). Also, rules around [[https://www.americanbar.org/groups/public_education/resources/law_related_education_network/how_courts_work/discovery/][Discovery]] prevent
  really recent discoveries from being used as an ambush.

** Siloing
After I asked myself these kinds of questions I then was reminded of
two instances in which a computer program was asked to interpret law
questions.

- In the podcast Opening Arguments, non-lawyer co-host Thomas Smith
  often attempts to answer bar exam questions presented by co-host and
  lawyer P. Andrew Torrez in order to communicate to the audience a
  little about the kind of qualifications lawyers must have in order
  to practice law. Recently, with the growing popularity of applying
  the chatbot [[https://en.wikipedia.org/wiki/ChatGPT][ChatGPT]] to difficult fields, Thomas Smith has been
  competing with ChatGPT to answer multiple-choice questions from a
  practice bar exam. So far, ChatGPT provides impressive-sounding
  responses with often relevant phrasing, but it has a poor history of
  actually choosing the correct multiple-choice answer.
- In [[https://openargs.com/oa686-the-robot-lawyers-are-coming-or-are-they/][Opening Arguments 686: The Robot Lawyers are Coming! …Or Are
  They?]], guest Liz Dye presented a startup centered around the idea of
  automating law at scale. From what I can tell the venture was mainly
  a publicity stunt to farm reputation so I won't describe it in
  detail. The point is, though, that various entities in social media
  shared it because the story has grown plausible enough to be
  entertaining to the general public.

This led me to te thought: if a language model like ChatGPT developed
enough to be able to perform all three activities (observe, integrate,
reticulate), then, if we were to encounter such a being on a distant
planet, we'd probably label them intelligent. Currently, Gizmodo
[[https://gizmodo.com/openai-chatgpt-ai-chat-bot-1850001021][reported]] that its develoers at OpenAI trained it using human responses
via Kenyan workers; this definitely strikes me as a combination of
**observation** and **integration** activity but the lack of real-time
interconnection between users (I have yet to hear about ChatGPT being
used as a messenger to deliver gossip between users or other copies of
itself) tells me the **reticulation** component is not really present.

The sign of reticulation is fast information interchange.

The sign of integration is generation of heat from computation.

The sign of observation is adaptability to a changing environment.

Currently, ChatGPT seems to have its observation abilities restricted
from its other activities: general users don't seem able to provide it
with much new information (probably for [[https://www.cbsnews.com/news/microsoft-shuts-down-ai-chatbot-after-it-turned-into-racist-nazi/][good reason]] this early on).

From what I understand [[https://ai.stackexchange.com/a/22881][from a quick scan of Stack Exchange]], a large
amount of graphics cards are required to run something like
ChatGPT. These parts which are likely to generate heat, would be where
it does its integration.

The fact that I can (and did) open a channel to talk with at least one
instance of ChatGPT to engage in real-time conversation (I fed it a
bunch of bash scripts I wrote and it seemed to give okay, if verbose
answers).






(TO WRITE: if one of the 3 purposes can be restricted, will it not ever develop intelligence?)





#+BEGIN_COMMENT
- Export his file to HTML in Emacs using C-c C-e h o

- When exporting this file to HTML, add the "#+OPTIONS: html-postamble:nil"
  line to the document to avoid including a W3C validation link.

- See the Org mode manual to understand the structure of this .org
  file ( https://orgmode.org/orgguide.pdf ).
#+END_COMMENT

** Subtitle

Example image here.

#+CAPTION: Example of an invalid RSS feed
#+NAME: fig:invalid_rss_feed
[[../../res/2021/img/example.png][../blog/res/2021/img/example.png]]

