<a id="org646ab8b"></a>

# Chemical Engineering applications using the Bitcoin blockchain

Created by [Steven Baltakatei Sandoval](http://baltakatei.com) on 2020-12-30T23:29Z under a [CC
BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) license and last updated on 2020-12-31T01:15Z.


<a id="orga51ba6c"></a>

## Summary

I composed the following post in response to a discussion [prompt](https://engage.aiche.org/communities/community-home/digestviewer/viewthread?GroupId=13&MessageKey=7995c9a5-ab84-4968-8d28-b336fee0bbc6) in
the AIChE Engage forum.


## Table of Contents

1.  [Chemical Engineering applications using the Bitcoin blockchain](#org646ab8b)
    1.  [Summary](#orga51ba6c)
    2.  [Prompt](#org581d72e)
    3.  [Reply](#orgc9f4e61)
        1.  [Idea 1. Historian archive integrity](#orgdf4fc8c)
        2.  [Idea 2. Micropayment-facilitated process miniaturization](#orgf182467)
        3.  [Blockchain inefficiency](#org5fa47b6)

<a id="org581d72e"></a>

## Prompt

**To**: Discussion Central

**Subject**: Blockchain technology

Blockchain technology is an upcoming trend when it comes to immutable
hack-proof digital technologies. Bitcoin is a classic example. I have
a strong feeling that blockchain could be the next big thing since
ARPANET become our INTERNET.

Anyone aware of the potential of this technology being implemented in
chemical process industries (CPI)&#x2026;

Ideas are welcome..

    ------------------------------
    Rohit Korde MSc,CEng
    Program Manager
    Worley
    Thane (W)
    ------------------------------


<a id="orgc9f4e61"></a>

## Reply

**To**: Discussion Central

**Subject**: RE: Blockchain technology

I run my own [Bitcoin node](https://bitcoin.org/en/full-node) and maintained a [Lightning](https://github.com/lightningnetwork/lnd) channel
before. I'm not as familiar with Ethereum or other [proof-of-stake](https://ethereum.org/en/developers/docs/consensus-mechanisms/pos/)
blockchains beyond knowing that they gain scalability and flexibility
over Bitcoin (which uses [proof-of-work](https://en.bitcoin.it/wiki/Proof_of_work)) at the cost of
decentralization. In contrast to its token price volatility, the
dominant (in terms of [market capitalization](https://support.coinmarketcap.com/hc/en-us/articles/360043836811-Market-Capitalization-Cryptoasset-Aggregate-)) [Bitcoin software](https://bitcoincore.org/) changes
less frequently over time. Therefore, some ideas I have regarding
applications may prove significant in the chemical process
industries. Although I use Bitcoin as an example, none of these ideas
requires involvement of any particular commercial entity or
blockchain, so long as a permission-less blockchain is used (i.e. one
based on proof-of-work). These ideas are influenced by time working as
a Plant Engineer / MOC Coordinator for a [small independent oil and gas
company](https://elkpetroleum.com/)'s carbon dioxide reinjection plant in southern Utah.


<a id="orgdf4fc8c"></a>

### Idea 1. Historian archive integrity

The idea is to prove the existence of process data using the Bitcoin
blockchain. This may be accomplished by software that periodically
(e.g. every 3 hours) constructs and submits a bitcoin transaction
(e.g. [OP<sub>RETURN</sub>](https://bitcoin.stackexchange.com/a/29555)) containing the merkle root of a [hash tree](https://en.wikipedia.org/wiki/Merkle_tree). Each leaf
of the tree is the cryptographic digest (i.e. "hash", e.g. [SHA256](https://en.wikipedia.org/wiki/SHA-2)) of
a small file.

Take, for example, a situation in which each leaf is a file containing
the time, equipment tag, and instrument reading of a gas composition
transmitter. Already there exist public APIs known as [calendar servers](https://alice.btc.calendar.opentimestamps.org/)
which accept files submitted through a [website](https://opentimestamps.org/) or [Python script](https://github.com/opentimestamps/opentimestamps-client). In
theory a process plant's historian could extend a calendar server's
merkle tree by creating a local hash tree every 3 hours from many
instrumentation readings across the entire facility and then
submitting as a file this tree's merkle root. The plant's historian
software could then maintain a database containing these
trees. Linking individual instrument readings in this way is useful
because it would then be possible to prove with a hard cryptographic
guarantee that particular sets of historian data existed at (or after)
a certain time. Additionally, the prover (e.g. a process engineer with
access to the merkle tree database) need not provide entire trees to a
verifier (e.g. an emissions compliance consultant); only a "merkle
branch" need be sent for each individual leaf of the tree, reducing
data bandwidth and storage requirements. Each merkle branch consists
of a set of cryptographic hashes of cryptographic hashes pointing from
the embedded bitcoin transaction (immutable) to a gas composition
reading. The novelty of this arrangement derives from two facts:

-   The Bitcoin blockchain did not exist until
    `2009-01-03T18:15:05+00:00`.
-   The Bitcoin blockchain is timestamped and immutable.

The bottom line is that the Bitcoin blockchain permits creation of
proofs that specific data existed in the past. This may be useful for
investigating/preventing rewriting of history that two parties may
disagree about (e.g. process data of a costly upset, fraudulent mill
test reports).


<a id="orgf182467"></a>

### Idea 2. Micropayment-facilitated process miniaturization

The idea is use blockchain micropayments to increase economic
viability of expensive process intensification that reduces the size
of a process plant. Cost savings comes from reducing the time (and
overhead costs) between an operations service company (e.g. equipment
maintenance, feedstock suppliers) performing a service and receiving
payment for that service. Additional revenue can be obtained by
installing small plants in locations where a larger one is not cost
effective.  For example, crude oil pumped from a marginal well in a
remote location today likely must be transported through a pipeline to
a distant refinery where unit operations may be applied to produce
petrol products useful to the human population near the wells. Due to
distance, the pipeline may prove too uneconomic to construct or
maintain. If a well owner installed a miniaturized refinery at each
individual well in order to bypass the need of a pipeline, the mundane
issue of the monthly billing cycle appears. A contract service company
may judge a single small well-refinery operation as not large enough
to justify allocating accountant time to process checks. In theory,
existing financial institutions equipped with modern computers have
the capability to facilitate micropayments but most efforts have
[floundered](https://cs.stanford.edu/people/eroberts/cs201/projects/2010-11/MicropaymentsAndTheNet/history.html).

In this example, the mini-refinery could directly pay
services providing maintenance labor and expendables (ex: TEG,
lubricant, refrigerant, etc.) through the Lightning Network (LN). For
a summary of how LN works with Bitcoin, I refer you to the background
section of [this](https://www.nature.com/articles/s41598-020-61137-5) 2020 [Scientific Reports](https://en.wikipedia.org/wiki/Scientific_Reports) paper. The bottom line is that
the mini-refinery could purchase service technician time or sell
dispensed product with transactions carrying fees as low as [0.00000001
BTC](https://1ml.com/statistics) (0.00029 USD) per transaction.

With such low transaction fees, sale of a product stream could be
performed nearly continuously; with the refinery's LN channel's
bitcoin balance state updated on a per-minute basis based on flowmeter
readings. A purchase order for technician time could be issued and
approved automatically for every minute the technician is on
site. Only when a refinery's LN channel balance becomes lopsided
(e.g. with its own earnings) will the refinery owner have to submit
anything to the blockchain. The channel can be closed at any time by
the owner or their LN channel partner since every LN transaction is
also a valid Bitcoin transaction. A customer receiving product
(e.g. gasoline) need not have a channel with the well owner in order
to purchase product, so long as a path of channels exists in the
Lightning Network connecting the customer to the owner.

The well-refinery in this example could be replaced with other
processes described in [this](https://www.aiche.org/conferences/aiche-annual-meeting/2019/proceeding/session/distributed-chemical-and-energy-processes-sustainability) session of the 2019 AIChE Annual Meeting
such as electrical power generation and biomass conversion to fuels.


<a id="org5fa47b6"></a>

### Blockchain inefficiency

I would warn anyone reviewing the applicability of "blockchain"
technology to beware of the fact that a blockchain is extremely
inefficient method of accounting compared to a centralized trusted
database. I would argue that utilizing a blockchain for accounting is
only justified in applications where parties cannot be trusted to
attempt to steal funds via [chargebacks](https://en.wikipedia.org/wiki/Chargeback) (e.g. a customer filling their
automobile's gasoline tank but later claiming the credit card used to
pay for the gasoline was stolen). Examples of blockchain inefficiency
include:

-   Data storage requirements. Blockchains maintain integrity of
    transaction history by recording every transaction ever
    performed. Due to this limitation, Bitcoin can only support
    approximately [3 to 7](https://www.nature.com/articles/s41598-020-61137-5) non-LN transactions per second; up to
    approximately 1 megabyte of new data must be permanently stored
    every 10 minutes. As of the latest block (number [663,744](https://chainflyer.bitflyer.com/Block/00000000000000000002f19cb758b9234767c6e74e8dd2d0a8c7d9b92c93174d)),
    362,023,310,633 bytes are required to store the Bitcoin blockchain.
-   Proof-of-work energy requirements. Permission-less blockchains
    (e.g. Bitcoin) achieve decentralized consensus by expending
    electrical energy to perform mathematical calculations. See [this](https://www.youtube.com/watch?v=bBC-nXj3Ng4)
    3Blue1Brown video for an explanation. [This](https://www.sciencedirect.com/science/article/abs/pii/S2542435120303317) article estimates Bitcoin
    power consumption to be on the order of 4 gigawatts in January 2020.
-   Time delay. In contrast to a centralized trusted database that can
    add a new record nearly instantaneously, Bitcoin transactions should
    not be considered finalized until at least an hour has passed; that
    is, until enough "[confirmations](https://bitcoin.stackexchange.com/a/160)" have occurred. This limit varies
    according to the rules of each blockchain type but is a function of
    the protocol's target time delay between each block of transactions
    (e.g. [10 minutes for Bitcoin, 2.5 minutes for Litecoin](https://bitinfocharts.com/comparison/confirmationtime-btc-ltc.html#3y), [image](https://imgur.com/4bMV19h.png)).

End.

    ------------------------------
    Steven Baltakatei Sandoval
    reboil.com
    Vancouver WA
    ------------------------------

