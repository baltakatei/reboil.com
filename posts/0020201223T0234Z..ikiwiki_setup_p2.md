<meta charset="utf-8">

# Ikiwiki Setup (part 2)

Created by [Steven Baltakatei Sandoval][bktei_2020_homepage] on
2020-12-23T02:35Z under a [CC BY-SA 4.0][cc_20131125_bysa] license and last
updated on 2020-12-23T15:15Z.

## Update

I figured out how to migrate the git repository containing my legacy
[reboil.com](https://web.archive.org/web/20201101105656/http://reboil.com/)
website (an [Amazon Web Services S3](https://aws.amazon.com/s3/)
bucket using Route 53 DNS routing (see
[this](https://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html)
reference)) to an ikiwiki blog (this blog post you should be reading
as of 2020-12-23). Using
[this](https://www.alexkras.com/git-merg-two-or-more-repositories-and-keeping-history/)
handy procedure, I found it to be possible to maintain a line of
continuity in git commit and log data in the git repository that the
default ikiwiki configuration on my
[Freedombox](https://www.freedombox.org/) uses.

I spent the past few hours figuring out how to get `ikiwiki`, `git`,
and `cron` to play nicely together so I can push blog posts like this
using a `git push` operation from my personal laptop without having to
login as root to my Freedombox via `ssh`. I think I have something
working. Even if my configuration changes in the near future, the
ability to version-control my blog posts gives me peace of mind that I
can preserve an roll back my blog's content.

Here are steps that I used to set this up.

## Setup Procedure

The setup basically consists of the following steps:

-   Create a `ikiwiki` blog.

-   Creating a `gitweb` bare repository to initially mirror the
    `ikiwiki` repository.

-   Push the initial `ikiwiki` repository `master` branch to the
    `gitweb` repo.

-   Add the `gitweb` repository as a remote in the `ikiwiki` repository.

-   Create a `root` user `cron` job on the Freedombox to periodically
    perform the required pushes, pulls, and `ikiwiki` rebuilds.

With all these preparations in place, it will then be possible to
create a new blog post via two methods:

1.  Creation, committing, and signing of a new blog post markdown file
    in a local clone of the `gitweb` repository on a personal computer.

2.  Creation or editing of a markdown file via `ikiwiki`'s webCGI
    interface. (note: the commit is not signed and may later be
    manually pulled from/merged/pushed to the `gitweb` repository).

The following sections will explain these steps in detail for a blog
named `blog-test`. Once a working setup is confirmed functional,
repeat the steps for a blog named `blog`.

1.  Setup `ikiwiki` application

    Install `ikiwiki` via the Freedombox plinth menus.
    
    Create a blog named `blog-test`. This will cause the URL of the blog to be
    shorter (i.e. `https://reboil.com/ikiwiki/blog-test`).

2.  Setup `gitweb` application

    Install `gitweb` via the Freedombox plinth menus.
    
    Create a git repository named `ikiwiki-blog-test`.

3.  Setup `ikiwiki` repository

    Create a backup git bundle of the blog's initial state via:
    
        $ sudo su -
        # pushd /var/lib/ikiwiki/blog-test
        # git bundle create ~/$(date --iso-8601=seconds)..blog-test.bundle --all
    
    Add the `gitweb` remote (a local directory) via:
    
        # git remote add localgitweb /var/lib/git/ikiwiki-blog-test.git
    
    The `ikiwiki` repo remote list should read:
    
        /var/lib/ikiwiki/blog-test# git remote -v
        localgitweb	/var/lib/git/ikiwiki-blog-test.git (fetch)
        localgitweb	/var/lib/git/ikiwiki-blog-test.git (push)
        origin	/var/lib/ikiwiki/blog-test.git (fetch)
        origin	/var/lib/ikiwiki/blog-test.git (push)
    
    The `ikiwiki` repo branch list should read:
    
        /var/lib/ikiwiki/blog-test# git branch --all
        * master
          remotes/localgitweb/master
          remotes/origin/master

4.  Setup `gitweb` repository

    In the Freedombox Plinth webUI, under Apps, under Gitweb, create a
    repository named `ikiwiki-blog-test`.
    
    From the `ikiwiki` repo working directory, push the blog `master`
    branch to the `gitweb` repo.
    
        # pushd /var/lib/ikiwiki/blog-test
        /var/lib/ikiwiki/blog-test# git push localgitweb master
        Enumerating objects: 12, done.
        Counting objects: 100% (12/12), done.
        Delta compression using up to 2 threads
        Compressing objects: 100% (11/11), done.
        Writing objects: 100% (12/12), 1.65 KiB | 141.00 KiB/s, done.
        Total 12 (delta 0), reused 0 (delta 0)
        To /var/lib/git/ikiwiki-blog-test.git
         * [new branch]      master -> master
    
    Create and push the `ikiwiki` repo's `master` branch to the
    `master` branch on the `gitweb` repo.
    
        # git push localgitweb master:master
    
    The `ikiwiki` repo branches should look something like this:
    
        /var/lib/ikiwiki/blog-test# git branch --all
        /var/lib/ikiwiki/blog# git branch --all 
          ikiwiki_revert_337dd2a3446701388bafc9616bccfaac659ca44a
        * master
          remotes/localgitweb/master
          remotes/origin/master

5.  Setup `root` user `cron` job

    Create a bash script file at `/root/ikiwiki_blog-test_update.sh`. The
    script should perform the following pseudocode functions:
    
        #!/bin/bash
        # Fetch updates
        echo "STATUS:Fetching updates." 1>&2
        pushd /var/lib/ikiwiki/blog-test
        git fetch --all
        git checkout master
        
        # Push ikiwiki repo 'master' to gitweb repo 'master'
        # Ref/Attrib: https://stackoverflow.com/a/3206144
        echo "STATUS:Pushing ikiwiki-master to gitweb-master." 1>&2
        git push localgitweb master:master
        
        # Pull gitweb repo 'master' to ikiwiki repo 'master'
        # Ref/Attrib: https://stackoverflow.com/a/8888015
        echo "STATUS:Pulling gitweb-master to ikiwiki-master" 1>&2
        git pull localgitweb master:master
        
        # Push changes
        echo "STATUS:Push to ikiwiki" 1>&2
        git push origin
        echo "STATUS:Rendering ikiwiki." 1>&2
        ikiwiki --setup /var/lib/ikiwiki/blog-test.setup --rebuild --verbose --gettime
        
        popd
    
    Create a `cron` job.
    
        # crontab -e
    
    Have it contain the following line.
    
        0 * * * * /bin/bash /root/ikiwiki_blog_update.sh 1>/dev/null 2>&1
    
    You may have to manually perform an initial merge, but the
    `/var/lib/ikiwiki/blog-test` ikiwiki git repository should now
    automatically synchronize with the
    `/var/lib/git/ikiwiki-blog-test.git` gitweb repository at the top of
    every hour.

## Summary

Using a Freedombox, `ikiwiki`, `git`, `gitweb`, and `cron`, it is
possible to set up a blog that can be updated remotely via `git push`.

[bktei_2020_homepage]: http://baltakatei.com
[cc_20131125_bysa]: http://creativecommons.org/licenses/by-sa/4.0/


<hr>
<p xmlns:dct="http://purl.org/dc/terms/" xmlns:cc="http://creativecommons.org/ns#">This work by <a rel="cc:attributionURL"  href="http://baltakatei.com"><span rel="cc:attributionName">Steven Baltakatei Sandoval</span></a> is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser" target="_blank" rel="license noopener noreferrer" style="display: inline-block;">CC BY-SA 4.0</a><a href="https://creativecommons.org/licenses/by-sa/4.0/?ref=ccchooser"><img style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-by_icon.svg" /><img  style="height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;" src="https://search.creativecommons.org/static/img/cc-sa_icon.svg" /></a></p>
