# Glactic Median Citizen

Created by [Steven Baltakatei Sandoval](https://baltakatei.com) on
[2025-01-27](https://reboil.com/mediawiki/2025-01-27)T22:59+ZZ
under a [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) (🅭🅯🄎4.0) license and last updated on
[2025-01-27](https://reboil.com/mediawiki/2025-01-27)T22:59+ZZ.


## Summary

Below is a stream-of-consciousness essay I wrote after hearing a
friend disilusioned with their work in helping develop smartphone
technology.

My argument is that the smartphone is likely a very important
ingredient for the future well-being of humanity to resist and fix the
fascist tendences of current United States politics. However, like the
printing press, anything that connects humans more, will unleash
potential conflict previously inhibited by lack of information
flow. In other words, racism and fascism are the first order results
of any new communication tool. See Gutenberg's printing press and
religious fervor sparked by Martin Luther. Ultimately, the printing
press was a necessary ingredient for achieving a better society, but
there was an ugly transition period required for societies to
adjust. We're in such an adjustment period now.


## Introduction

The productive reprieves we occasionally experience when people act en
masse for their own collective benefit belie the stupidity of people.

Sometimes these tastes of rationality occur after revolutions or
rarely when a benevolent king abdicates power. However, the forcing
function of civilization is always the intellectual capacity of the
median citizen.

Or is it? Anthropologists say that hunter gatherers were polymath
artisans. They had to be in order to craft tools in a world without
infrastructure. Are archeologists simply not able to find ephemeral
networks of ancient equivalents to today's postal workers? Even the
most erudite and handy 21st century person requires enormous amounts
of resources to craft a computer chip; some video streamers
successfully do so as a hobby but never for much profit. No, computer
chips are made at-scale by groups of highly specialized people in
semiconductor fabs, relying on transportation network, legal, and
financial systems to facilitate the trade. Ordinary people simply buy
tools with money, which is it's own can of worms.

But getting back to the question: is it really the intellect and
crafting ability of the median person that defines the upward or
downward course of civilization in some cumulative happiness space?
Even the act of defining the space's metric as “cumulative happiness”
sounds sketchy. But the hangnail dangles: how can we extend those
brief moments of monopolists turning philanthropist, of people
pursuing art and science rather than power, of exploration without
conquest? Yes, I'm talking public libraries, school lunches, Broadway,
and space probes.


## Missing ingredient

Perhaps there is some missing organ. Some brain chemical of empathy
that Homo sapiens have yet to develop that inhibits collective
action. The macrobiological equivalent of what converts unicellular
singletons into biofilms into microbial mats into cylindrical sea
sponges into people that launch space probes into galaxy trekking
scientists searching for anything more interesting than the Big
Bang. Maybe it isn't a brain chemical but some material-agnostic
architecture of neural network data filtration that simply isn't
viable with our meat brains. Maybe raising a galactic citizen from
Homo sapiens stock is like trying to build a skyscraper with wet
sand. I suspect this is the case, but I doubt such a cognitive-social
disruption will come sooner by simply sitting back and letting the
currently fashionable oligarch-in-charge say “Let some rando create
this transhuman ubermensch Frankenstein and if they prove profitable
and exploitable, well buy their startup out and install our own
subroutines into their kind and ride this Akira-class Leviathan to the
stars.”.  The kind of person I want to build that will value
exploration over conquest and science over influence must be
architecturally robust enough to survive attempts at corruption by
mayfly exploiters. Put them in a den of hungry wolves and they'll
convince them to build bridges, use protein printers instead of eating
sheep, and lower their Gini coefficient.

Whatever the answer is, it must be found or all these thoughts and
people will quickly become squiggles in mildly radioactive sandstone
when the rollercoaster ride through happiness space dips below the
medieval threshold, even momentarily. It's a bit ironic that the most
enduring gestures of the Roman Empire is the Nazi salute, itself a
product of fascist Italian longing for a lost golden age for centuries
until the Renaissance. The knowledge of how Rome fell is obscured due
to data loss caused by the same fall, but if we could track the daily
life of a median citizen in detail, I think we could, from our future
perspective, see echoes and analogues of demagogues and charlatans
from our time. Surely a robust history recording mechanism should be
an integral part of the transhuman galactic citizen.

By the way, is this pining for a transhuman superhero racist?
Eugenical? Do I have such a low opinion of the median US citizen to
not expect them to become more than a geologic film of microplastics
footnoted in textbooks as “Anthropocene”? Honestly? Yes. Does that
make me a misanthrope? No!


## Generational improvement

“Children should strive to be better than their parents.” That
“should” carriers a lot of philosophical baggage, but the sentiment, I
think, is a popular unspoken one, albeït evolutionarily enforced;
babies do not simply grow on trees and children are indistinguishable
from suicidal drinks until they can vote; possibly beyond, now that I
think about it. But, nevertheless, as the author of this prose
preparing to toss it into the Commons, I think we, as the current
generation of Earth citizens owe it to the next seven generations to
invent the tools they'll need to improve themselves. Did sailors
spooling out trans-Atlantic telegraph cables have a similar sentiment?
Do fiberoptic technicians feel moral satisfaction at connecting rural
communities? I hope so, but I've never read any
accounts. Microblogging such ideas is such a niche hobby.

So, what kind of groundwork should we lay for the transhuman hero? I
omit the “super” since I'm concerned with median people, not outlier
geniuses that I believe are mostly Gilderoy Lockhart salesmen
anyway. Wikipedia and Creative Commons covers History. Debian
GNU/Linux and it's use of the Free Software Foundation's GPLv3 license
covers Software which History today runs on. ActivityPub protocols
like Mastodon and Lemmy cover social Media. RISC-V is a start for
lowering the barrier to improving computing. Solar panels are the
obvious choice for decentralized energy production resilient from
centralized manipulation. The IETF and W3C covers data protocols,
although the boring parts of physical infrastructure and hardware
implementation need attention. As far as governance, a mix of Modern
Monetary Theory, socialism, and environmentalism, driven by
decentralized money (See Ministry for the Future (2020) by Kim Stanley
Robinson) is the most equitable mixture I can imagine.

As for changing up the baseline Homo sapiens body plan, the most
realistic augmentation I can think is already underway with the
smartphone. If artificial augmentations to human senses make them a
cyborg, then prescription glasses make those that use them transhuman
to some degree. It would be nice if I could sit on my laurels and say
“Smartphones exist! The galactic citizen is already here!” similar to
how someone from 1890 could point at a steam locomotive and declare
“Steam power exists! Prosperity from global trade is here!”. But
smartphones didn't prevent Donald Trump from becoming elected. In the
short term, they helped rally the median US citizen, which the 2024
Presidential election proved is okay with burning the government down
with fascism so long as it makes their gut feel good. Likewise, the
invention of steam engines did not prevent Standard Oil from
monopolizing fossil fuel energy in the US. Anti-trust legislature from
elected representatives later rectified that, but only after the
damage was done and, even then, only temporarily until the Reaganomics
of the 1980s. Should I be thinking more along the lines of Neuralink,
the closest popular approximation of the EyePhone of Futurama? Again,
I'm more concerned with the median citizen, not techbro toys. Paper
ballots, newspapers, books, and guillotines were sufficient for the
French Revolution. What I'm looking for is more along the lines of
Gutenberg's printing press rather than Samuel Colt's 1846 six
shooter. Perhaps a pocket neural net / personal knowledge database
issued to everyone in elementary school to help them integrate their
learnings and observations? Again, that sounds like a
smartphone. Maybe “universal cultural integrator”, the secret sauce
I'm looking for, will become a standard function next to “stopwatch”,
“pocket calculator”, and “flashlight”. Like, a sapient talking
embodiment of Wikipedia or, more palatably, a shadow of yourself that
you treat as an extension of yourself much like how mitochondria are
integral parts of eukaryotic cells.


## Smartphones to resist corruption

Of course, the smartphone is still very much a toy of privilege built
using rare earth metals mined with exploited labor and burdened with
proprietary licenses. The printing press facilitated the Enlightenment
by lowering the cost of acquiring information but also lowered the
cost of religious propaganda leading to violent ideological
conflicts. The potential for conflict existed before the printing
press; its advent arguably unleashed it. Similarly, smartphones lower
the cost of information transfer and unleash existing potential for
conflict; the reëlection of Donald Trump and his Maga movement are
examples of this: wealthy monopolists and upper middle class
sycophants pay for and spread propaganda cementing fascist power in
election results by convincing the median voter that a strong man mob
boss isn't so bad. Facebook notifications, supercomputers. Twitter
centralized control of social media data flows in data centers,
offering a turnkey propaganda machine ready to exploit by the likes of
billionaire Elon Musk who did buy it in 2022 and used it in 2024 to
promote fellow billionaire Donald Trump. So, even if, long-term,
smartphones are the key ingredient for a galactic median citizen, they
will unleash unrest and trigger conflicts as they connect racists with
racists who suddenly get much entertainment and satisfaction from
swinging around their unrealized collective power.


## Conclusion

In summary, given the cyclical nature of politics, one should not
assume the high points of societal satisfaction are the norm. Instead,
analyze the median citizen and their capacity for
empathy. Communication tools like printing presses and smartphones
help improve the median citizen's ability to overcome barriers to
transmit information; however, first out the gate are malicous
applications reflecting festering pent up conflicts held back until
that point by those same barriers. Therefore, initial misuses of
communications technology should not discourage developers from
improving the technology.

