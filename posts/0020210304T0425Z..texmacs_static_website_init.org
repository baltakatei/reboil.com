* TeXmacs Static Website
#+TITLE: TeXmacs Static Website
#+AUTHOR: Steven Baltakatei Sandoval
#+DATE:YYYY-mm-dd
#+EMAIL:baltakatei@gmail.com
#+LANGUAGE: en
#+OPTIONS: toc:nil

Created by [[http://baltakatei.com][Steven Baltakatei Sandoval]] on 2021-03-04T04:26Z under a [[https://creativecommons.org/licenses/by-sa/4.0/][CC
BY-SA 4.0]] license and last updated on 2021-03-04T06:06Z.

#+BEGIN_COMMENT
- Export his file to HTML in Emacs using C-c C-e h o

- When exporting this file to HTML, add the "#+OPTIONS: html-postamble:nil"
  line to the document to avoid including a W3C validation link.

- See the Org mode manual to understand the structure of this .org
  file ( https://orgmode.org/orgguide.pdf ).
#+END_COMMENT

** Summary
I created a static website using TeXmacs. It can be found [[https://reboil.com/texmacs/][here]].

** Background
I rewrote [[https://reboil.com/texmacs/articles/0020190815T064636Z..hancke-kuhn_dbp_explanation.xhtml][an older blog post about a distance-bounding protocol]] that I
authored in markdown with MathML tags. The math typesetting features
of TeXmacs along with its static website generator and default CSS
settings made for a much nicer-looking site.

I used the [[https://texmacs.github.io/notes/docs/website-builder-dialog.html][Notes on TeXmacs]] blog as a template for some features,
although I didn't use all the Schema features (that other blog
automatically updates an ATOM feed among other things). Redirecting
from ~index.html~ to another page was a feature I used. I may adopt
the Schema macros and some CSS, but even with just the bare TeXmacs
website generator settings, it [[../blog/res/2021/img/20210304T0405Z..reboil_first_texmacs_article.png][looks pretty good]]!
