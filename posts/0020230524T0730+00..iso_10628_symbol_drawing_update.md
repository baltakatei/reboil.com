# ISO 10628 symbol drawing update

Created by [Steven Baltakatei Sandoval](https://baltakatei.com) on
[2023-05-24](https://reboil.com/mediawiki/2023-05-24)T07:30+00
under a [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) (🅭🅯🄎4.0) license and last updated on
[2023-05-31](https://reboil.com/mediawiki/2023-05-31)T19:55+00.

![img](https://reboil.com/res/2023/img/20230524T105507+00_BK-2020-04-PID-1-SHT2..sample_view_300x387px.png "A sample view of `BK-2020-04-PID-1`")

Edit([2023-05-31](https://reboil.com/mediawiki/2023-05-31)):Add reboil.com wiki links.


## Summary

On [2023-05-23](https://reboil.com/mediawiki/2023-05#2023-05-23), I updated a drawing I made in [2020-09](https://reboil.com/mediawiki/2020-09) to serve as a
palette for chemical engineering symbols I might use when constructing
my own [P&ID](https://reboil.com/mediawiki/Piping_and_instrumentation_diagram)s.


## Background

Back in [2020-09](https://reboil.com/mediawiki/2020-09), I became interested in creating P&ID diagrams for use
in my personal notes and in the [DeVoe's Thermodynamics and Chemistry](https://gitlab.com/baltakatei/bk-2021-07-1)
transcription project of mine ([BK-2021-07](https://reboil.com/mediawiki/BK-2021-07)). I decided to find an
industry consensus standard set of symbols so that my drawings (which
I planned to license CC BY-SA 4.0 for use in Wikipedia and Wikimedia
Commons) could be used by other people. Therefore, I purchased a copy
of [`ISO 10628-2:2012`](https://www.iso.org/standard/51841.html) and manually drew each symbol in [Inkscape](https://inkscape.org/).

On [2020-09-25](https://reboil.com/mediawiki/2020-09-25), I uploaded a set of drawings showing all the `ISO
10628-2:2012` symbols to [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) (See [BK-2020-04-PID-1-SHT1](https://commons.wikimedia.org/wiki/File:ISO_10628-2_2012_Symbols_Sheet_1.svg)
[SVG](https://en.wikipedia.org/wiki/SVG) file). I split the upload into several separate SVG files, due to
lack of multi-page support for SVGs in Inkscape. I also uploaded a set
of [PDF](https://en.wikipedia.org/wiki/PDF) files exported from Inkscape since the [`ISO 3098`](https://reboil.com/mediawiki/ISO_3098) font I chose
for the drawing ([osifont](https://tracker.debian.org/pkg/fonts-osifont) in the Debian repository) wasn't supported by
Wikimedia Commons.

In [2020-02](https://reboil.com/mediawiki/2020-02), I received an email from a John Kunicek about the symbol
numbering system used in `ISO 10628` symbols I drew in
`BK-2020-04-PID-1`. They also informed me of some spelling errors. On
2023-02-27, I reviewed Kunicek's questions and came to the conclusion
that basically the symbol numbers in `ISO 10628` followed a pattern
established in another ISO standard called `ISO 14617` of which [`ISO
14617-1:2005`](https://www.iso.org/standard/41838.html) is an index of registration numbers for symbols used in
other ISO standards such as ISO 10628. On 2023-02-28 I sent my reply
to Kunicek and updated the `BK-2020-04-PID-1` legends to address the
ambiguities of the original legend that was limited to what
information was provided in my copy of `ISO 10628:2012`. 

On [2020-03-01](https://reboil.com/mediawiki/2020-03-01), while I had the files on my mind, I also decided to
edit the SVG files of `BK-2020-04-PID-1` so that each symbol and its
associated text objects containing ISO 10628 descriptions and
registration numbers were grouped together; this update would enable
someone to easily copy and paste individual symbols if they edited the
drawing in Inkscape or, if a machine were performing a text search of
the body of the SVG file itself, they could quickly find the
registration numbers to help them identify the associated symbols
nearby in the XML tree of the SVG file. Previously, the symbol objects
and text objects were mixed together in a hodgepodge; symbols and
their associated text objects were only obviously related to a human
looking at the rendered image. I didn't push the updated SVG files
since I wanted to wait and see if any more corrections or questions
came from Kunicek or others.

On [2021-05-08](https://reboil.com/mediawiki/2021-05-08), Wikipedia editor [Lonaowna](https://en.wikipedia.org/wiki/User:Lonaowna) [added](https://en.wikipedia.org/w/index.php?title=ISO_10628&diff=1022132213&oldid=958604116) thumbnails of the SVGs
I uploaded to the [ISO 10628](https://en.wikipedia.org/wiki/ISO_10628) Wikipedia article. I myself hadn't wanted
to do so since I felt it would have been a conflict of interest and
rather self-promotional to push edits containing links of my
self-published works.

On [2023-05-23](https://reboil.com/mediawiki/2020-05-23), I decided to go ahead and upload to Wikimedia Commons
the updates of the `BK-2020-04-PID-1` SVG files containing the
corrections and clarifications I applied in [2020-02](https://reboil.com/mediawiki/2020-02)/[2020-03](https://reboil.com/mediawiki/2020-03). I also
made some adjustments to text placement since, annoyingly, Wikimedia
Commons doesn't have an `ISO 3098` compliant font for use in technical
drawing SVG files; PNG previews of the uploaded SVGs showed text
converted into a generic sans serif font that is 25% wider than that
used by **osifont**. After some adjustments and reuploads, I ended up
with satisfying SVG and PDF versions uploaded to Wikimedia
Commons. Again, [here](https://commons.wikimedia.org/wiki/File:ISO_10628-2_2012_Symbols_Sheet_1.svg) is a link to the first sheet of
`BK-2020-04-PID-1`; the description contains links to the other PDFs
and SVGs for all the sheets.


## Motivation

A significant amount of legwork is involved in creating a reference
drawing when none meeting your criteria exists already. My criteria is
that works I publish should be compatible with a Creative Commons
BY-SA 4.0 license; creating `BK-2020-04-PID-1`, a palette of ISO 10628
symbols, was part of that process for me. I hope that the work I put
in helps save other people time which they can dedicate to leisure. I
believe leisurely people are the most capable of creative works. I
would prefer to live in a world where projects such as Wikipedia and
Wikimedia Commons can expand in scope to cover specialized
disciplines, saving people time from reinventing wheels.

Even if my uploaded work is hoovered up by an AI and spit out mixed
with others, my carefully crafted drawings will remain causally
upstream of the process; I think AI language models such as [ChatGPT](https://en.wikipedia.org/wiki/ChatGPT)
will lubricate on-demand knowledge downloads for the public; the
quality of those downloads is dependent upon the quality of the data
set the language models are trained on. I'm okay with this process and
hope to find other like-minded people who are willing to make a living
making such contributions to common knowledge without the politics of
worrying about using copyright to protect trade secrets.

